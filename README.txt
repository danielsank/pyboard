pyboard is a library for making network board games.

The library is based on the idea of remote objects. You write classes
implementing a variety of in-game objects by subclassing classes provided
by pyboard. You can then simply pass game objects to clients playing the
game. The client side objects receive notifications whenever the objects
undergo relevant activity, and those changes can be presented by a GUI. A
critical design feature is that the game objects do not have to know
anything about the particular GUI being used.

Notable features include:

1. Lobby (with chat) included.

2. Pluggable game system: Once you've written a new game, you put the
   main object in a list and all players connected to the server will see
   that game available in the lobby.

3. Network level coding is mostly done for you.

4. Working example games with PyQt GUIs included.


Installation
------------

pyboard is in development and cannot yet be "installed." However, if you
download the source you can run the examples.


News
----

See the file NEWS for the user-visible changes from previous releases.


License
------

See the LICENSE file.


Downloading
-----------

observed can be obtained via git

https://bitbucket.org/danielsank/pyboard


Documentation
-------------

The main classes are documented. Documentation is ongoing.


Development
-----------

observed development is hosted on bitbucket. The current working
repository is given in the Downloading section above.


Bug Reporting
-------------

Report bugs on the bitbucket ticket tracker.

