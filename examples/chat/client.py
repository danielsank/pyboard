"""
Copyright (c) 2014 Daniel Sank

This file is part of pyboard, an open source library for playing board games
over the network.

This file contains classes for establishing a client connection to the server.
"""

import twisted.spread.pb as pb
import twisted.cred.credentials as credentials

import pyboard.util.cacheableTypes as ct
import pyboard.examples.chat.chatWindow as chatWindow

#Get setUnjellyableForClass definitions
import pyboard.examples.chat.chat as chat

#Named functions
from twisted.internet.defer import inlineCallbacks

class Client(pb.Referenceable):
    """I am the mind"""
    
    def __init__(self, cxn):
        self.perspective = None
        self.cxn = cxn
        self.factory = pb.PBClientFactory()
        self.window = chatWindow.ChatWindow()
    
    def connect(self, password):
        c = credentials.UsernamePassword(self.cxn.username,
                                         password)
        from twisted.internet import reactor
        reactor.connectTCP(self.cxn.host, self.cxn.port, self.factory)
        #Log in with self as the "mind"
        d = self.factory.login(c, self)
        d.addCallback(self.connected)
        # No errback here; let Connect's errback handle failed login.
        return d
    
    @inlineCallbacks
    def connected(self, perspective):
        if perspective:
            self.perspective = perspective
            chat = yield self.perspective.callRemote("getChat")
            self.gotChat(chat)
    
    def gotChat(self, chat):
        self.chat = chat
        self.window.gotLogical(chat)

