"""
Copyright (c) 2014 Daniel Sank

This file is part of pyboard, an open source library for playing board games
over the network.

This is the main launcher file for game clients.
"""

import sys

import PyQt4.QtGui as QtGui
import PyQt4.QtCore as QtCore
import pyboard.util.qt4reactor as qt4reactor

import connect

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    qt4reactor.install()
    from twisted.internet import reactor
    connector = connect.Connect()
    reactor.run()
