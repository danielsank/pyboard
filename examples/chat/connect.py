import constants as C

import PyQt4.QtGui as QtGui
import PyQt4.QtCore as QtCore
import PyQt4.uic as uic

import client

class PyQt4ConnectGraphics(QtGui.QWidget):
    def __init__(self, logical, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.logical = logical
        
        self.ui = uic.loadUi('connect.ui')
        self.ui.pushButtonConnect.clicked.connect(self.connect)
        self.ui.lineEditPort.setText(str(C.PORT))
        self.ui.lineEditHost.setText('localhost')
        self.ui.show()
    
    def connect(self):
        port = int(self.ui.lineEditPort.text())
        host = str(self.ui.lineEditHost.text())
        username = str(self.ui.lineEditUsername.text())
        password = str(self.ui.lineEditPassword.text())
        self.logical.connect(host, port, username, password)
    

class Connect(object):
    
    GraphicsClass = PyQt4ConnectGraphics
    
    def __init__(self):
        self.graphics = self.GraphicsClass(self)
        self.host = None
        self.port = None
        self.username = None
        
    def connect(self, host, port, username, password):
        self.host = host
        self.port = port
        self.username = username
        c = client.Client(self)
        d = c.connect(password)
        d.addCallback(self.connected)
        #d.addErrback(lambda e: print("Failed to connect: %s"%e))
    
    def connected(self, result):
        self.graphics.ui.hide()
    
