"""
Copyright (c) 2014 Daniel Sank

This file is part of pyboard, an open source library for playing board games
over the network.

This file provides classes for a chat channel.
"""

import twisted.spread.pb as pb

import pyboard.util.networkModel as nm

from pyboard.util.observed import event


class ChatControl(nm.Control):
    
    def __init__(self, obj):
        nm.Control.__init__(self, obj)
    
    def view_say(self, perspective, message):
        """Invoked remotely by a user to say something"""
        self.obj.said(perspective, message)


class ChatModel(nm.Model):
    
    controlClass = ChatControl
    
    def __init__(self):
        nm.Model.__init__(self)
    
    def stateForPerspective(self, perspective):
        return {} #ChatModel comes with no state
    
    def said(self, sayer, message):
        """Notify all remote observes that someone said something"""
        for o in self.observers.values():
            o.callRemote("said", "%s: %s"%(sayer.name, message))


class ChatView(nm.View):
    
    def receiveState(self, state):
        pass #No state to receive
    
    @event
    def observe_said(self, message):
        """
        Called by server when someone says something
        
        We implement no logic here, but graphics elements can sign up to be
        notified when this method runs so that they can modify the display.
        """
        pass
    
pb.setUnjellyableForClass(ChatModel, ChatView)

