"""
Copyright (c) 2014 Daniel Sank

This file is part of pyboard, an open source library for playing board games
over the network.

This is a main window implementation using PyQt.
"""

import os

import PyQt4.QtGui as QtGui
import PyQt4.uic as uic

class ChatWidget(QtGui.QWidget):
    """A chat window"""
    
    def __init__(self, *args, **kw):
        super(ChatWidget, self).__init__(*args, **kw)
        self.logical = None
        self.initUi()
    
    def initUi(self):
        uic.loadUi(os.path.join(os.path.dirname(__file__),"chatWidget.ui"),\
            self)
        self.input.returnPressed.connect(self.sendMessage)
        self.show()
    
    def gotLogical(self, chatLogical):
        self.logical = chatLogical
        self.logical.observe_said.addObserver(self.receiveMessage)
    
    def receiveMessage(self, message):
        self.output.append(message)
    
    def sendMessage(self):
        message = str(self.input.text())
        self.input.clear()
        if self.logical:
            self.logical.callRemote("say", message)

