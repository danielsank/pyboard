"""
Copyright (c) 2014 Daniel Sank

This file is part of pyboard, an open source library for playing board games
over the network.

This is a main window implementation using PyQt.

TODO:
We should probabily factor out the logic from the main window into an interface
to be used by various graphical implementations.
"""

import twisted.internet.defer as defer

import PyQt4.QtGui as QtGui
import PyQt4.uic as uic

import pyboard.examples.chat.chat as chat

class ChatWindow(QtGui.QMainWindow):
    """A chat window"""
    
    def __init__(self, *args, **kw):
        super(ChatWindow, self).__init__(*args, **kw)
        self.chat = None
        self.initUi()
    
    def initUi(self):
        uic.loadUi("chatWindow.ui", self)
        self.input.returnPressed.connect(self.sendMessage)
        self.show()
    
    def gotLogical(self, chatObject):
        self.chat = chatObject
        self.chat.observe_said.addObserver(self.receiveMessage)
    
    def receiveMessage(self, message):
        self.output.append(message)
    
    def sendMessage(self):
        message = str(self.input.text())
        self.input.clear()
        if self.chat:
            self.chat.callRemote("say", message)

