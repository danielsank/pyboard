import twisted.spread.pb as pb

from twisted.internet.defer import inlineCallbacks
from twisted.internet import reactor
from twisted.cred import credentials

class Client(pb.Referenceable):

    def connect(self):
        factory = pb.PBClientFactory()
        reactor.connectTCP("localhost", 8800, factory)
        def1 = factory.login(credentials.UsernamePassword("alice", "1234"),
                             client=self)
        def1.addCallback(self.connected)
    
    def remote_takeViewable(self, v, message):
        v.callRemote("test", "I got an un-asked for MyViewable %s"%(message,))
    
    def remote_takeCacheable(self, c):
        print("Client got a Cacheable of type %s"%(c.__class__,))
    
    @inlineCallbacks
    def connected(self, perspective):
        self.perspective = perspective
        v1 = yield perspective.callRemote("getViewable")
        v2 = yield v1.callRemote("test", "I got an asked-for MyViewable")
        yield v2.callRemote("test", "I got an asked-for MyOtherViewable returned by a MyViewable")
        c = yield perspective.callRemote("getCacheable")
        reactor.stop()