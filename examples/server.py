import twisted.spread.pb as pb
import twisted.cred.portal as portal
import twisted.spread.jelly as jelly

from zope.interface import implements


class Server(object):
    implements(portal.IRealm)
    
    def __init__(self):
        self.v = MyViewable()
        self.c = MyCacheable( description="passed as arg", makeSub=True)
    
    def requestAvatar(self, avatarID, mind, *interfaces):
        assert pb.IPerspective in interfaces
        p = Perspective(avatarID, mind)
        mind.perspective = p
        mind.callRemote("takeViewable", self.v, "passed as pure arg")
        #mind.callRemote("takeViewable", pb.ViewPoint(p, self.v), "passed as ViewPoint")
        mind.callRemote("takeCacheable", self.c)
        return pb.IPerspective, p, p.logout
    

class Perspective(pb.Avatar):
    def __init__(self, name, mind):
        self.name = name
        self.mind = mind
        self.v = MyViewable()
        self.c = MyCacheable("passed as return value")
        print("Perspective made with id: %d"%id(self))
    
    def detached(self):
        self.mind = None
    
    def perspective_getViewable(self):
        return self.v
    
    def perspective_getCacheable(self):
        return self.c
    
    def logout(self):
        self.detached()
    

class MyViewable(pb.Viewable):
    def __init__(self):
        self.v = MyOtherViewable()
    
    def view_test(self, perspective, msg):
        if perspective is not None:
            name = perspective.name
        else:
            name = "None"
        print("%s: %s"%(name, msg))
        return self.v
    

class MyOtherViewable(pb.Viewable):
    
    def view_test(self, perspective, msg):
        name = perspective.name if perspective.name is not None else "None"
        print("%s: %s"%(perspective.name, msg))
    

class MyCacheable(pb.Cacheable):
    def __init__(self, description=None, makeSub=False):
        self.observers = set()
        self.foo = 0
        self.description = description
        if makeSub:
            self.c = MyCacheable(description=\
                "passed as state of another Cacheable",
                makeSub=False)
        else:
            self.c = None
    
    def getStateToCacheAndObserveFor(self, perspective, observer):
        name = perspective.name if perspective.name is not None else "None"
        print("%s observing MyCacheable %s"%(name, self.description))
        self.observers.add(observer)
        return {'foo':self.foo, 'description':self.description,
                'c':self.c}
    
    def setFoo(self, val):
        self.foo = val
        for o in self.observers:
            o.callRemote("setFoo", val)
    

class MyRemoteCache(pb.RemoteCache):
    
    def setCopyableState(self, state):
        for k, v in state.items():
            self.__dict__[k] = v
    
    def observe_setFoo(self, val):
        print("%s foo observed set to %s"%(self.description, val))
    

pb.setUnjellyableForClass(MyCacheable, MyRemoteCache)