import os
import weakref

import PyQt4.QtGui as QtGui
import PyQt4.QtCore as QtCore
import PyQt4.uic as uic

class Game(QtGui.QWidget):
    
    def __init__(self, parent):
        QtGui.QWidget.__init__(self, parent)
        self.game = None
        self.initUi()
    
    def initUi(self):
        uic.loadUi(os.path.join(os.path.dirname(__file__),"game.ui"),\
            self)
    
    def gotGame(self, game):
        self.game = weakref.proxy(game)
        self.hand.gotHand(game.hand)
        self.deck.gotDeck(game.deck)


class Deck(QtGui.QWidget):
    
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
    
    def gotDeck(self, deck):
        self.deck = weakref.proxy(deck)
        deck.setLen.addObserver(self.setLen)
    
    def setLen(self, n):
        print("Deck len set to %d"%n)
    
    def mousePressEvent(self, e):
        print("You clicked on the deck")
        QtGui.QWidget.mousePressEvent(self, e)
        self.deck.callRemote("draw")


class Hand(QtGui.QGraphicsView):
    
    def __init__(self, parent=None):
        QtGui.QGraphicsView.__init__(self, parent)
        self.scene = QtGui.QGraphicsScene(0, 0, 300, 300, self)
        self.setScene(self.scene)
        self.hand = None
    
    def gotHand(self, hand):
        print("Hand got hand of length %d"%len(hand))
        self.hand = weakref.proxy(hand)
        hand.add.addObserver(self.add)
        for card in hand:
            self.add(card)
    
    def add(self, card):
        self.scene.addItem(CardGraphics(self, card))


class CardGraphics(QtGui.QGraphicsPixmapItem):
    """A card that you can move around"""
    
    def __init__(self, parent, logical):
        pixmap = QtGui.QPixmap(logical.imgPath)
        pixmap = pixmap.scaledToWidth(150, QtCore.Qt.SmoothTransformation)
        QtGui.QGraphicsPixmapItem.__init__(self, pixmap)
        self.setCursor(QtCore.Qt.OpenHandCursor)
        self.setFlag(QtGui.QGraphicsItem.ItemIsMovable, True)
        self.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, True)
        self.setAcceptedMouseButtons(QtCore.Qt.LeftButton)
        
        self.logical = weakref.proxy(logical)
        self.logical.observe_setLocation.addObserver(self.setLocation)
        self.setLocation(logical.location)
        
    def mousePressEvent(self, e):
        QtGui.QGraphicsPixmapItem.mousePressEvent(self, e)
        self.setCursor(QtCore.Qt.ClosedHandCursor)
        
    def mouseReleaseEvent(self, e):
        QtGui.QGraphicsPixmapItem.mouseReleaseEvent(self, e)
        self.setCursor(QtCore.Qt.OpenHandCursor)
        self.logical.requestSetLocation((self.scenePos().x(),
                                         self.scenePos().y()))
        
    def setLocation(self, location):
        self.setPos(*location)
        self.update()
    
