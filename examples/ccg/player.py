import pyboard.util.observed as observed

class Player(object):
    def doThing(self, x):
        print("Player did a thing, %x"%(x,))


class NetworkPlayer(Player, observed.ObservedRedux):
    
    remoteMethodNames = ["doThing"]
    
    def __init__(self):
        observed.ObservedRedux.__init__(self)
        self.addObserver("doThing", self.onDoThing)
    
    def onDoThing(self, x):
        print("Observed that thing was done with %s"%(x,))

