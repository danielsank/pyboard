import os
import weakref

import twisted.spread.pb as pb

import pyboard.util.networkModel as nm
import pyboard.util.cacheableTypes as ct

from pyboard.util.observed import event


class GameControl(nm.Control):
    
    def __init__(self, obj):
        nm.Control.__init__(self, obj)


class GameModel(nm.Model):
    
    controlClass = GameControl
    
    def __init__(self, obj):
        nm.Model.__init__(self, obj)
    
    def stateForPerspective(self, perspective):
        state = {}
        state['name'] = self.obj.name
        state['hand'] = self.obj.hand.model
        state['deck'] = self.obj.deck.model
        state['type'] = self.obj.type
        return state


class GameView(nm.View):
    """Client side game object"""
    
    def receiveState(self, state):
        self.name = state['name']
        self.hand = state['hand']
        self.deck = state['deck']
        self.type = state['type']


class Game(nm.Networkable):
    
    modelClass = GameModel
    
    def __init__(self, name, creator, password):
        nm.Networkable.__init__(self)
        self.name = name
        self.type = 'ccg'
        self.hand = Hand()
        self.deck = Deck('Bob')
        self.creator = creator
        self.passwordHash = hash(password)
        
        self.deck.drawn.addObserver(self.hand.add)
        
        self.hand.add(Card("King", os.path.join(os.path.dirname(__file__),\
            "king.jpg")))
        self.deck.add(Card("Ace", os.path.join(os.path.dirname(__file__),\
            "ace.jpg")))
        self.deck.add(Card("Queen", os.path.join(os.path.dirname(__file__),\
            "queen.png")))
    
    def authenticate(self, user, password):
        if hash(password) == self.passwordHash:
            return True
        else:
            return False


pb.setUnjellyableForClass(GameModel, GameView)


class CardControl(nm.Control):
    
    def __init__(self, obj):
        nm.Control.__init__(self, obj)
    
    def view_setLocation(self, perspective, location):
        self.obj.setLocation(location)
    
    def view_setOrientation(self, perspective, orientation):
        self.obj.setOrientation(orientation)


class CardModel(nm.Model):
    
    controlClass = CardControl
    
    def __init__(self, obj):
        nm.Model.__init__(self, obj)
        self.obj.setLocation.addObserver(self.setLocation)
    
    def setLocation(self, location):
        for p in self.observers:
            self.observers[p].callRemote("setLocation", location)
    
    def setOrientation(self, orientation):
        print("Card orientation not implemented")
    
    def stateForPerspective(self, perspective):
        return {'name': self.obj.name,
                'location': self.obj.location,
                'orientation': self.obj.orientation,
                'imgPath': self.obj.imgPath}


class CardView(nm.View):
    
    def receiveState(self, state):
        self.name = state['name']
        self.location = state['location']
        self.imgPath = state['imgPath']
    
    @event
    def observe_setLocation(self, location):
        self.location = location
    
    def requestSetLocation(self, location):
        self.callRemote("setLocation", location)


class Card(nm.Networkable):
    
    modelClass = CardModel
    
    def __init__(self, name, imgPath):
        nm.Networkable.__init__(self)
        self.name = name
        self.imgPath = imgPath
        self.location = (0, 0)
        self.orientation = 0.0
    
    @event
    def setName(self, name):
        self.name = name
    
    @event
    def setLocation(self, location):
        self.location = location
    
    def remodel(self):
        nm.Networkable.remodel(self)


pb.setUnjellyableForClass(CardModel, CardView)


class HandControl(nm.Control):
    
    def __init__(self, obj):
        nm.Control.__init__(self, obj)


class HandModel(nm.Model):
    
    controlClass = HandControl
    
    def __init__(self, obj):
        nm.Model.__init__(self, obj)
        obj.add.addObserver(self.add)
        obj.discard.addObserver(self.discard)
    
    def add(self, card):
        for p in self.observers:
            self.observers[p].callRemote("add", card.model)
    
    def discard(self, obj):
        for p in self.observers:
            self.observers[p].callRemote("discard", obj.model)
    
    def stateForPerspective(self, perspective):
        state = {}
        state['contents'] = set([c.model for c in self.obj])
        return state


class HandView(nm.View, ct.ObservableSet):
    
    def __init__(self):
        ct.ObservableSet.__init__(self)
    
    def receiveState(self, state):
        self._s = state['contents']
    
    def observe_add(self, obj):
        self.add(obj)
    
    def observe_discard(self, obj):
        self.discard(obj)


class Hand(ct.ObservableSet, nm.Networkable):
    
    modelClass = HandModel
    
    def __init__(self):
        ct.ObservableSet.__init__(self)
        nm.Networkable.__init__(self)
    
    def remodel(self):
        nm.Networkable.remodel(self)
        for c in self:
            c.remodel()
    
    def __hash__(self):
        print("pyboard.ex.ccg.game.Hand.__hash__ called")
        return hash(id(self))
    
    def __eq__(self, other):
        return id(self) == id(other)


pb.setUnjellyableForClass(HandModel, HandView)


class DeckControl(nm.Control):
    
    def view_shuffle(self, perspective):
        if 'Bob' == self.obj.owner:
            print("Warning: need to fix DeckControl")
            self.obj.shuffle()
    
    def view_draw(self, perspective, n=1):
        if 'Bob' == self.obj.owner:
            print("Warning: need to fix DeckControl")
            self.obj.draw(n=n)


class DeckModel(nm.Model):
    
    controlClass = DeckControl
    
    def __init__(self, obj):
        nm.Model.__init__(self, obj)
        
        obj.add.addObserver(self.add)
        obj.drawn.addObserver(self.drawn)
        obj.shuffle.addObserver(self.shuffle)
    
    def stateForPerspective(self, perspective):
        state = {}
        state['len'] = len(self.obj)
        return state
    
    def add(self, card):
        for p in self.observers:
            self.observers[p].callRemote("add")
    
    def drawn(self, card):
        for p in self.observers:
            self.observers[p].callRemote("drawn")
    
    def shuffle(self):
        for p in self.observers:
            self.observers[p].callRemote("shuffle")


class DeckView(nm.View, ct.ObservableList):
    
    def receiveState(self, state):
        self.len = state['len']
    
    def observe_add(self):
        self.setLen(self.len + 1)
    
    def observe_drawn(self):
        print("Card was drawn")
        self.setLen(self.len - 1)
    
    @event
    def observe_shuffle(self):
        print("Deck was shuffled")
    
    @event
    def setLen(self, n):
        self.len = n


class Deck(nm.Networkable, ct.ListBase):
    
    modelClass = DeckModel
    
    def __init__(self, owner):
        nm.Networkable.__init__(self)
        ct.ListBase.__init__(self)
        self.owner = owner
    
    @event
    def add(self, card):
        self.insert(0, card)
    
    def draw(self, n=1):
        for _ in range(n):
            if len(self) > 0:
                card = self.pop(0)
                self.drawn(card)
    
    @event
    def drawn(self, card):
        pass
    
    @event
    def shuffle(self):
        print("You need to implement deck shuffling")


pb.setUnjellyableForClass(DeckModel, DeckView)


