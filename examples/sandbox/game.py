import os

import twisted.spread.pb as pb

import PyQt4.QtGui as QtGui
import PyQt4.QtCore as QtCore
import PyQt4.uic as uic

import pyboard.util.networkModel as nm
import pyboard.util.cacheableTypes as ct
import pyboard.util.observed as observed

from pyboard.util.observed import event

class GameControl(nm.Control):
    
    def view_requestNewSquare(self, perspective):
        self.obj.newSquare()


class GameModel(nm.Model):
    
    controlClass = GameControl
    
    def __init__(self, name, creator, password):
        """"""
        nm.Model.__init__(self)
        self.name = name
        self.type = 'sandbox'
        self.creator = creator
        self.password = hash(password)
        self.players = set()
        self.squares = ct.CacheableSet()
    
    def authenticate(self, user, password):
        if hash(password) == self.password:
            return True
        else:
            return False
    
    def stateForPerspective(self, perspective):
        state = {}
        state['squares'] = self.squares
        state['name'] = self.name
        state['type'] = self.type
        return state
    
    def newSquare(self):
        s = SquareModel()
        self.squares.add(s)


class GameView(nm.View):
    """Client side game object"""
    
    def receiveState(self, state):
        for k, v in state.items():
            setattr(self, k, v)
    
    def requestNewSquare(self):
        """Ask for creation of a new square"""
        return self.callRemote("requestNewSquare")


pb.setUnjellyableForClass(GameModel, GameView)


class SquareControl(nm.Control):
    
    def view_setLocation(self, perspective, location):
        self.obj.setLocation(location)


class SquareModel(nm.Model):
    
    controlClass = SquareControl
    
    def __init__(self):
        nm.Model.__init__(self)
        self.location = (0, 0)
    
    def setLocation(self, location):
        self.location = location
        for o in self.observers.values():
            o.callRemote("setLocation", location)
    
    def stateForPerspective(self, perspective):
        return {'location': self.location}


class SquareView(nm.View):
    
    def receiveState(self, state):
        self.location = state['location']
    
    @event
    def observe_setLocation(self, location):
        self.location = location
    
    def requestSetLocation(self, location):
        self.callRemote("setLocation", location)


pb.setUnjellyableForClass(SquareModel, SquareView)


class GameWindow(QtGui.QWidget):
    def __init__(self, parent):
        super(GameWindow, self).__init__(parent)
        uic.loadUi(os.path.join(os.path.dirname(__file__),"game.ui"), self)
        self.newSquareButton.clicked.connect(self.requestNewSquare)
    
    def gotGame(self, game):
        self.game = game
        self.playingField.gotGame(game)
    
    def requestNewSquare(self):
        self.game.callRemote("requestNewSquare")


class PlayingField(QtGui.QGraphicsView):
    def __init__(self, parent):
        super(PlayingField, self).__init__(parent)
        self.scene = QtGui.QGraphicsScene(0, 0, 300, 300, self)
        self.setScene(self.scene)
    
    def gotGame(self, game):
        game.squares.add.addObserver(self.newSquare)
        for s in game.squares:
            self.newSquare(s)
    
    def newSquare(self, square):
        squareGraphics = SquareGraphics(self, square)
        self.scene.addItem(squareGraphics)


class SquareGraphics(QtGui.QGraphicsPixmapItem):
    """A square that you can move around"""
    def __init__(self, parent, logical):
        pixmap = QtGui.QPixmap(os.path.join(os.path.dirname(__file__),\
            "square.png"))
        pixmap = pixmap.scaledToWidth(50, QtCore.Qt.SmoothTransformation)
        QtGui.QGraphicsPixmapItem.__init__(self, pixmap)
        self.setCursor(QtCore.Qt.OpenHandCursor)
        self.setFlag(QtGui.QGraphicsItem.ItemIsMovable, True)
        self.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, True)
        self.setAcceptedMouseButtons(QtCore.Qt.LeftButton)
        
        self.logical=logical
        self.logical.observe_setLocation.addObserver(self.setLocation)
        self.setLocation(logical.location)
        
    def mousePressEvent(self, e):
        QtGui.QGraphicsPixmapItem.mousePressEvent(self, e)
        self.setCursor(QtCore.Qt.ClosedHandCursor)
        
    def mouseReleaseEvent(self, e):
        QtGui.QGraphicsPixmapItem.mouseReleaseEvent(self, e)
        self.setCursor(QtCore.Qt.OpenHandCursor)
        self.logical.requestSetLocation((self.scenePos().x(),
                                         self.scenePos().y()))
        
    def setLocation(self, location):
        self.setPos(*location)
        self.update()
    
