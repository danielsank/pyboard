class Player(object):
    def __init__(self, name, game):
        """A single player in a particular game
        
        Note that a Player is different from a User. A single User may play
        multiple games simultaneously and so can correspond to more than one
        Player.
        """
        self.name = name
        self.game = game
    