import client
import server
from twisted.internet import reactor

def main():
    c = client.Client()
    c.connect()
    reactor.run()

if __name__ == "__main__":
    main()
