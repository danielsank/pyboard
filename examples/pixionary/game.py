import twisted.spread.pb as pb

import pyboard.util.viewableCacheable as vc
import pyboard.util.cacheableTypes as ct
import pyboard.util.observed as observed

from pyboard.util.observed import event

class GameModel(vc.State):
    
    def __init__(self, stateOf, *arg, **kw):
        """"""
        vc.State.__init__(self, stateOf)
        self.pixels = ct.CacheableList()
        for i in range(numCols*numRows):
            self.pixels.append(Pixel())
    
    def populateStateDict(self, state, perspective):
        state['pixels'] = self.pixels


class GameControl(vc.ViewableWithCachedState):
    
    stateClass = GameModel


class GameRemoteCache(vc.StateRemoteCache):
    """Client side game object"""
    
    def receiveState(self, state):
        self.pixels = state['pixels']
        self.name = state['name']


pb.setUnjellyableForClass(GameModel, GameRemoteCache)


class PixelModel(vc.State):
    def __init__(self, stateOf, *arg, **kw):
        vc.State.__init__(self, stateOf)
        self.color = <default color>
    
    def setColor(self, color):
        self.color = color
        for o in self.observers:
            o.callRemote("setColor", color)
    
    def populateStateDict(self, state, perspective):
        state['color'] = self.color


class PixelControl(vc.ViewableWithCachedState):
    
    stateClass = PixelModel
    
    def view_setColor(self, perspective, color):
        if perspective.isDrawer():
            self.setColor(color)


class PixelRemoteCache(vc.StateRemoteCache, observed.Observed):
    def __init__(self):
        observed.Observed.__init__(self)
    
    def receiveState(self, state):
        self.color = state['color']
    
    @event
    def observe_setColor(self, color):
        self.color = color


pb.setUnjellyableForClass(Pixel, PixelRemoteCache)


class PlayingField(<Some Kivy base class>):
    # <Kivy code>
    pass


class PixelGraphics(<Some Kivy base class, Josh did this>):
    """A square that you can move around"""
    # <Kivy code>
    pass

