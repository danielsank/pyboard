from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import NumericProperty, ReferenceListProperty,\
    ObjectProperty
from kivy.graphics import Color, Ellipse, Line, Rectangle
import operator
import colorList

red = [1,0,0,1]
green = [0,1,0,1]
blue =  [0,0,1,1]
purple = [1,0,1,1]
black=[0,0,0,0]


class PixelRect(Button):
    setColor = []
    def __init__(self,background_color=black,**kwargs):
        super(PixelRect,self).__init__(**kwargs)
        self.background_color=background_color
        
    def on_touch_down(self, touch):
        print "pressed"
        
        if self.collide_point(*touch.pos):
            print "collided"
            # if the touch collides with our widget, let's grab it
            touch.grab(self)
            print self.background_color
            PixelRect.setColor = self.background_color
            print "The selected color is ", PixelRect.setColor
            # Obj.color = self.background_color
            # and accept the touch.
            return True
    def on_touch_move(self,touch):
        pass
    def on_touch_up(self, touch):
        # here, you don't check if the touch collides or things like that.
        # you just need to check if it's a grabbed touch event
        # X = PixelBlock(pos = touch.pos) 
        if self.collide_point(*touch.pos):
            self.background_color=PixelRect.setColor
            print "The current color is ", PixelRect.setColor
            print "touch pos ",touch.pos

        if touch.grab_current is self:
            print "ungrabbed"
            touch.ungrab(self)

        # # and accept the last up
            return True
# 
class PixelArea(GridLayout):
    #Will play with the colors of this later
    pass
    
class PaletteArea(BoxLayout):    
    #This is just some fucking rectangle that highlights the palette area
    def __init__(self,**kwargs):
        super(PaletteArea, self).__init__(**kwargs)
        with self.canvas:
            Color(1,1,1)

            
class PaletteApp(App):
    def build(self):
        paletteGame = BoxLayout(orientation='vertical') #The whole layout
        layout = PixelArea(cols=20,rows=25,size_hint=(1,0.9)) #The area with the pixels
        paletteGame.add_widget(layout)
        for i in range(layout.cols*layout.rows):
            layout.add_widget(PixelRect())

        pa = PaletteArea(orientation='horizontal',size_hint=(1,0.1)) #The area with the color palette
        paletteGame.add_widget(pa)
        for i,col in enumerate(colorList.colors):
            pa.add_widget(PixelRect(background_color=col))
        return paletteGame

 
if __name__ == '__main__':
    PaletteApp().run()