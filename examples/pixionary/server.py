"""
Copyright (c) 2014 Daniel Sank

This file is part of pyboard, an open source library for playing board games
over the network.

This file defines the game Server and User objects.
"""

import logging

import pyboard.examples.pixtionary.constants as C
import pyboard.examples.pixtionary.game as game

import twisted.spread.pb as pb
import twisted.cred.portal as portal
import twisted.internet.defer as defer

#Named functions
from twisted.internet import reactor
from twisted.cred.error import LoginDenied
from twisted.cred.checkers import FilePasswordDB
from zope.interface import implements

class User(pb.Avatar):
    """Perspective for a single human user."""
    def __init__(self, name, server, mind=None):
        """Set up a new User
        
        Each user is described by a name.
        
        The User has several important attributes:
        server: The server to which this User is connected
        lobby: The game lobby in which the User currently sits
        mind: A remote reference to the client. If this is None it means the
              user is not currently logged in.
        """
        self.name = name
        self.server = server
        self.mind = mind
    
    def attached(self, mind):
        """Bind this user to a mind
        
        In order to be able to pass properly working Viewables and Cacheables
        as arguments to callRemote methods on the mind, the mind must know
        which perspective to use for serialization. We ensure this by setting
        mind's perspective attribute to be this User.
        """
        self.mind = mind
        # The next line allows us to send pb objects to the mind, and have
        # remote methods called _by_ the mind have the perspective argument
        # properly passed in.
        mind.perspective = self
    
    def detached(self):
        self.mind = None
    
    def logout(self):
        print("Logging out")
        self.lobby.logout(self)
        self.server.logout(self)
    
    def perspective_getGame(self):
        return self.server.game
    

class Server(object):
    implements(portal.IRealm)
    """I manage games and produce Avatars"""
    def __init__(self):
        self.initLogging()
        self.users = {} #username -> User
        self.game = game.Game()
        self.logger.info("Server running")
    
    def initLogging(self):
        self.logger = logging.getLogger("pyboard.%s"%str(self))
        self.logger.setLevel(logging.DEBUG)
        console = logging.StreamHandler()
        console.setLevel(logging.DEBUG)
        f = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
        console.setFormatter(f)
        self.logger.addHandler(console)

    def requestAvatar(self, avatarId, mind, *interfaces):
        """Called automagically by perspective broker stuff"""
        assert pb.IPerspective in interfaces
        user = self.users.setdefault(avatarId, User(avatarId, self, mind))
        user.attached(mind)
        user.isLoggedIn = True #Change to "attached"?
        return pb.IPerspective, user, user.logout
    
    def logout(self, user):
        self.users.pop(user.name)
    

class UniqueFilePasswordDB(FilePasswordDB):
    """A FilePasswordDB allowing only unique users"""

    def __init__(self, filename, delim=':', usernameField=0, passwordField=1,
                 caseSensitive=True, hash=None, cache=False, server=None):
        FilePasswordDB.__init__(self, filename, delim, usernameField,
                                passwordField, caseSensitive, hash, cache)
        self.server = server

    def requestAvatarId(self, c):
        if c.username in self.server.users:
            if self.server.users[c.username].mind is not None:
                return defer.fail(LoginDenied())
        else:
            return FilePasswordDB.requestAvatarId(self, c)
    

class UniqueNoPassword(UniqueFilePasswordDB):
    """Checker with no password and unique users only.
    
    Accepts any username
    """

    def requestAvatarId(self, credentials):
        if credentials.username in self.server.users and \
                self.server.users[credentials.username].mind is not None:
            return defer.fail(LoginDenied())
        else:
            #print("Checker giving %s"%(defer.succeed(credentials.username),))
            return defer.succeed(credentials.username)
