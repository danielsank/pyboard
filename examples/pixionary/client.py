import twisted.spread.pb as pb
import twisted.cred.credentials as credentials
import constants as C

from twisted.internet.defer import inlineCallbacks

class Client(pb.Referenceable):
    """I am the mind"""
    
    def __init__(self):
        self.perspective = None
        self.host = C.HOST
        self.port = C.PORT
        self.factory = pb.PBClientFactory()
        self.mainWindow = mainWindow.MainWindow(self)
        self.username = raw_input("Username: ")
        password = raw_input("Password: ")
        self.connect(password)
        
    def connect(self, password):
        c = credentials.UsernamePassword(self.username,
                                         password)
        from twisted.internet import reactor
        reactor.connectTCP(self.host, self.port, self.factory)
        #Log in with self as the "mind"
        d = self.factory.login(c, self)
        d.addCallback(self.connected)
        return d
    
    @inlineCallbacks
    def connected(self, perspective):
        """
        Called when we receive a perspective onto the system
        
        Think of the perspective argument as an object representing you,
        the user, on the network. You can invoke perspective_* methods on it
        to make things happen on the server.
        """
        if perspective:
            self.perspective = perspective
            game = yield perspective.callRemote("getGame")
            self.gotGame()
    
    def gotGame(self, game):
        print("Client received game object")
        print("Game contains %d pixels"%len(game.pixels))