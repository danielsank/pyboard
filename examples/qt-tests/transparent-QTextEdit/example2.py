from PyQt4 import QtGui, QtCore
import sys

class Main(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(Main, self).__init__(parent)
        
        frame = QtGui.QFrame(parent=self)
        frame.setStyleSheet("QFrame {background: rgba(255,255,255,20%)}")
        box=QtGui.QHBoxLayout()
        
        edit = QtGui.QTextEdit()
        edit.setStyleSheet("background: rgba(0,0,123,20%)")
        box.addWidget(edit)
        
        # edit2=QtGui.QTextEdit()
        # edit2.setStyleSheet("background: rgb(255,0,0)")
        # box.addWidget(edit2)
        
        frame.setLayout(box)
        
        pushbutton = QtGui.QPushButton('Quit')
        pushbutton.clicked.connect(self.close)
        box.addWidget(pushbutton)
        
        self.setCentralWidget(frame)


if __name__ == '__main__':

    app = QtGui.QApplication(sys.argv)
    main = Main()
    main.show()

    app.exec_()