import PyQt4.QtGui as QtGui
import PyQt4.QtCore as QtCore
import PyQt4.uic as uic

Qt = QtCore.Qt

class MainWindow(QtGui.QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        uic.loadUi("mainWindow.ui", self)
        self.saveButton.clicked.connect(self.save)
    
    def save(self):
        img = QtGui.QPixmap.grabWidget(self.markupWidget)
        img.save("test.png")


class MarkupWidget(QtGui.QWidget):
    def __init__(self, parent):
        super(MarkupWidget, self).__init__()
        uic.loadUi("markupWidget.ui", self)
        
        #self.rules.setAlignment(QtCore.Qt.AlignCenter)
        
        #image = QtGui.QPixmap("<some file>.jpg")
        #self.label.setPixmap(image)
        self.show()
        
        # effect = QtGui.QGraphicsDropShadowEffect()
        # effect.setBlurRadius(1)
        # effect.setColor(QtGui.QColor(0, 0, 0))
        # effect.setOffset(QtCore.QPointF(1,1))
        # self.textEdit.setGraphicsEffect(effect);


if __name__ == "__main__":
    import  sys

    app = QtGui.QApplication(sys.argv)
    main = MainWindow()
    main.show()
    sys.exit(app.exec_())
