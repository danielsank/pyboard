import PyQt4.QtGui as QtGui
import PyQt4.QtCore as QtCore
import PyQt4.Qt as Qt

import sys

DISPLAY_ROLE = Qt.Qt.DisplayRole
USER_DATA_ROLE = Qt.Qt.UserRole + 1

app = QtGui.QApplication(sys.argv)

class MyView(QtGui.QListView):
    
    #def __init__(self, parent=None):
    #    super(MyView, self).__init__(parent)
    
    def setModel(self, model):
        super(QtGui.QListView, self).setModel(model)
        self.selectionModel().selectionChanged.connect( \
            self.onChangedSelection)
    
    def onChangedSelection(self, newSelection, oldSelection):
        idx = self.selectionModel().currentIndex()
        item = self.model().itemFromIndex(idx)
        obj = item.data(USER_DATA_ROLE).toPyObject()
        print("The full object: %s"%str(obj))

view = MyView()
view.setMaximumSize(400,300)

model = QtGui.QStandardItemModel(view)

data = [
    ('Dan', 28),
    ('Anna', 27),
    ]

labels = ['First', 'Second']

for d in data:
    item = QtGui.QStandardItem()
    item.setData(d, USER_DATA_ROLE)
    item.setText(d[0])
    model.appendRow(item)

view.setModel(model)
view.show()
app.exec_()