#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from PyQt4 import QtCore, QtGui


class MovablePixmapItem(QtGui.QGraphicsPixmapItem):
    def __init__(self, pixmap, *args, **kwargs):
        QtGui.QGraphicsPixmapItem.__init__(self, pixmap, *args, **kwargs)
        self.setFlags(QtGui.QGraphicsItem.ItemIsMovable |
                      QtGui.QGraphicsItem.ItemSendsGeometryChanges)

    def itemChange(self, change, value):
        #if change == QtGui.QGraphicsItem.ItemPositionHasChanged:
        if change == QtGui.QGraphicsItem.ItemPositionChange:
            print value.toPointF()
            #Restrict motion
            return QtCore.QPointF(self.pos().x(), value.toPointF().y())
            # do other work, or emit signal
        return QtGui.QGraphicsPixmapItem.itemChange(self, change, value)


class MainWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.pixmapItem = MovablePixmapItem(QtGui.QPixmap('square.png'))
        self.scene = QtGui.QGraphicsScene()
        self.scene.addItem(self.pixmapItem)
        self.view = QtGui.QGraphicsView(self.scene)
        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.view)
        self.setLayout(layout)

    def do_test(self):
        pass


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    widget = MainWidget()
    widget.resize(640, 480)
    widget.show()
    sys.exit(app.exec_())