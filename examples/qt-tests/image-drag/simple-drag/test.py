import sys
from PyQt4 import QtGui, QtCore
from PyQt4 import uic

ACCEPT = {True: "accepted", False: "not accepted"}

class MainWindowUi(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.ui = uic.loadUi('ui.ui')
        self.scene = Scene(0, 0, 300, 300, self)
        self.ui.view.setScene(self.scene)
        self.ui.view.setFrameShape(QtGui.QFrame.NoFrame)
        
        pixmap = QtGui.QPixmap("square.png")
        pixmap = pixmap.scaledToWidth(50, QtCore.Qt.SmoothTransformation)
        pixItem = Square(pixmap)
        
        self.scene.addItem(pixItem)
    
class Scene(QtGui.QGraphicsScene):
    def __init__(self, x, y, w, h, parent):
        QtGui.QGraphicsScene.__init__(self, x, y, w, h, parent)
    
    def mousePressEvent(self, e):
        print("Scene got a mouse press event: %s")%(ACCEPT[e.isAccepted()],)
        print("Found %s"%(self.itemAt(e.scenePos()),))
        QtGui.QGraphicsScene.mousePressEvent(self, e)
    
    def mouseReleaseEvent(self, e):
        print("Scene got a mouse release event: %s")%(ACCEPT[e.isAccepted()],)
        QtGui.QGraphicsScene.mouseReleaseEvent(self, e)
    
class Square(QtGui.QGraphicsPixmapItem):
    def __init__(self, pixmap):
        QtGui.QGraphicsPixmapItem.__init__(self, pixmap)
        self.setCursor(QtCore.Qt.OpenHandCursor)
        self.setFlag(QtGui.QGraphicsItem.ItemIsMovable, True)
        self.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, True)
        self.setAcceptedMouseButtons(QtCore.Qt.LeftButton)
    
    def mousePressEvent(self, e):
        print("Square got a mouse press event: %s")%(ACCEPT[e.isAccepted()],)
        QtGui.QGraphicsPixmapItem.mousePressEvent(self, e)
        self.setCursor(QtCore.Qt.ClosedHandCursor)
        print(self.scenePos())
    
    def mouseReleaseEvent(self, e):
        print("Square got a mouse release event: %s")%(ACCEPT[e.isAccepted()],)
        QtGui.QGraphicsPixmapItem.mouseReleaseEvent(self, e)
        self.setCursor(QtCore.Qt.OpenHandCursor)
    
if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    win = MainWindowUi()
    win.ui.show()
    sys.exit(app.exec_())