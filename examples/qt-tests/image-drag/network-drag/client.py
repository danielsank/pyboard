gameNo = int(raw_input("What game do you want to play [int]: "))

import sys

if __name__ == '__main__':
    import pyboard.qt4reactor as qt4reactor
    qt4reactor.install()

from twisted.internet.defer import inlineCallbacks, returnValue
import twisted.spread.pb as pb

import PyQt4.QtGui as QtGui

import constants as C
import game

def main(ID, host=C.HOST, port=C.PORT):
    try:
        gameClient = game.GameClient(ID)
        factory = pb.PBClientFactory()
        reactor.connectTCP(host, port, factory)
        d = factory.getRootObject()
        d.addCallback(gameClient.gotServer)
        reactor.run()
    except:
        raw_input()

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    from twisted.internet import reactor
    main(gameNo)