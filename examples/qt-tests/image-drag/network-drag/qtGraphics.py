import PyQt4.QtGui as QtGui
import PyQt4.QtCore as QtCore
import PyQt4.uic as uic

class Game(QtGui.QMainWindow):
    def __init__(self, logical):
        QtGui.QMainWindow.__init__(self)
        self.logical = logical
        self.ui = uic.loadUi('game.ui')
        self.scene = QtGui.QGraphicsScene(0, 0, 300, 300)
        self.ui.view.setScene(self.scene)        
        self.ui.show()
        self.setupSignals()
    
    def setupSignals(self):
        self.ui.newSquareButton.clicked.connect(self.logical.requestSquare)
    
class Square(QtGui.QGraphicsPixmapItem):
    
    def __init__(self, logical):
        pixmap = QtGui.QPixmap("square.png")
        pixmap = pixmap.scaledToWidth(50, QtCore.Qt.SmoothTransformation)
        QtGui.QGraphicsPixmapItem.__init__(self, pixmap)
        self.logical = logical
        self.setCursor(QtCore.Qt.OpenHandCursor)
        self.setFlag(QtGui.QGraphicsItem.ItemIsMovable, True)
        self.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, True)
    
    def mousePressEvent(self, e):
        QtGui.QGraphicsPixmapItem.mousePressEvent(self, e)
        self.setCursor(QtCore.Qt.ClosedHandCursor)
    
    def mouseReleaseEvent(self, e):
        QtGui.QGraphicsPixmapItem.mouseReleaseEvent(self, e)
        self.setCursor(QtCore.Qt.OpenHandCursor)
        loc = self.scenePos()
        loc = (loc.x(), loc.y())
        self.logical.game.requestMoveSquare(self.logical, loc)
    