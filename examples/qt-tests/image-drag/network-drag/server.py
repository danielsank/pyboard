import twisted.internet.reactor as reactor
import twisted.spread.pb as pb

import pyboard.server as server
import game
import constants as C

class Server(server.Server):
    gameServerClass = game.GameServer

def main(port):
    reactor.listenTCP(port, pb.PBServerFactory(Server()))
    reactor.run()
    
if __name__ == '__main__':
    main(C.PORT)