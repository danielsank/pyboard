"""
pyboard example file
AUTHOR: Daniel Sank
CREATED: 18 October 2013
PURPOSE: Illustrate detection of objects moved by dragging, and notification
accross the network of that move.
LISCENSE:
"""

import twisted.spread.pb as pb
import PyQt4.QtCore as QtCore

import pyboard.game as game
import qtGraphics as graphics

class GameServer(game.GameServer):
    
    def __init__(self, *args, **kw):
        game.GameServer.__init__(self, *args, **kw)
        print(self.clients)
        self.squares = set()
    
    def cullState(self, state):
        return state
    
    def remote_makeSquare(self):
        sq = SquareMaster(0, 0)
        self.squares.add(sq)
        for c in self.clients:
            c.callRemote("takeSquare", sq)
    
    def remote_moveSquare(self, sq, loc):
        sq.setLocation(loc)
    
class GameClient(game.GameClient):
    
    graphicsClass = graphics.Game
    
    def processState(self, state):
        for k,v in state.items():
            self.__dict__[k] = v # o.O
        #This is bad coding for two reasons
        #1. We shouldn't have to special case graphics item processing.
        #   We're doing this so that late joining clients get all of the
        #   current graphics properly installed into their self.graphics.scene,
        #   but this is way to Qt specific.
        #2. We _definitely_ shouldn't be accessing members of self.graphics,
        #   which is what's happening in remote_takeSquare
        for sq in self.squares:
            self.remote_takeSquare(sq)
    
    def requestSquare(self):
        self.server.callRemote("makeSquare")
    
    def requestMoveSquare(self, sq, loc):
        self.server.callRemote("moveSquare", sq, loc)
    
    def remote_takeSquare(self, sq):
        self.squares.add(sq)
        self.graphics.scene.addItem(sq.graphics)
        sq.game = self
    
class SquareMaster(pb.Cacheable):
    def __init__(self, x, y):
        self.observers = set()
        self.location = (x, y)
    
    def getStateToCacheAndObserveFor(self, perspective, observer):
        self.observers.add(observer)
        d = {}
        d['location'] = self.location
        return d
    
    def setLocation(self, loc):
        self.location = loc
        for o in self.observers:
            o.callRemote("setLocation", loc)
    
class SquareSlave(pb.RemoteCache):
    
    def setCopyableState(self, state):
        print("A new slave appeared")
        self.location = state['location']
        self.graphics = graphics.Square(self)
        self.graphics.setPos(*self.location)
        self.graphics.update()
    
    def observe_setLocation(self, loc):
        self.location = loc
        self.graphics.setPos(*loc)
        self.graphics.update()
    
pb.setUnjellyableForClass(SquareMaster, SquareSlave) 