import twisted.spread.pb as pb
import twisted.cred.portal as portal

import pyboard.util.cacheableTypes as ct

from twisted.internet import reactor
from twisted.internet.defer import inlineCallbacks
from twisted.cred import credentials
from zope.interface import implements

class Server(object):
    implements(portal.IRealm)
    
    def __init__(self):
        self.d = ct.CacheableKeysMap()
        self.d['thingy'] = 'foo'
        self.d[None] = 98
        self.l = ct.CacheableList()
        self.l.append('this')
        self.l.append('is')
    
    def requestAvatar(self, avatarID, mind, *interfaces):
        assert pb.IPerspective in interfaces
        p = Perspective(avatarID, mind, self)
        return pb.IPerspective, p, lambda a=p:a.detached()
    

class Perspective(pb.Avatar):
    def __init__(self, name, mind, server):
        self.name = name
        self.mind = mind
        self.server = server
    
    def detached(self):
        self.mind = None
    
    def perspective_getDict(self):
        return self.server.d
    
    def perspective_getList(self):
        return self.server.l
    
    def perspective_readyForUpdates(self):
        print("Setting 'new' key")
        self.server.d["new"] = 100
        print("Appending 'nice'")
        self.server.l.append('nice')
        return True

class Client(pb.Referenceable):

    def connect(self):
        factory = pb.PBClientFactory()
        reactor.connectTCP("localhost", 8800, factory)
        def1 = factory.login(credentials.UsernamePassword("alice", "1234"),
                             client=self)
        def1.addCallback(self.connected)
    
    @inlineCallbacks
    def connected(self, perspective):
        self.perspective = perspective
        d = yield perspective.callRemote("getDict")
        for k in d:
            print k
        l = yield perspective.callRemote("getList")
        for elem in l:
            print(elem)
        yield perspective.callRemote("readyForUpdates")
        for k in d:
            print k
        for elem in l:
            print(elem)
        #reactor.stop()