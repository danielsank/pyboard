import pyboard.test.testCacheableTypes as tct
from twisted.internet import reactor

def main():
    c = tct.Client()
    c.connect()
    reactor.run()

if __name__ == "__main__":
    main()
