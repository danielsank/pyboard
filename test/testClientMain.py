import pyboard.test.test as test
from twisted.internet import reactor

def main():
    c = test.Client()
    c.connect()
    reactor.run()

if __name__ == "__main__":
    main()
