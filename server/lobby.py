"""
Copyright (c) 2014 Daniel Sank

This file is part of pyboard, an open source library for playing board games
over the network.

This file contains classes for the game lobby. A lobby is like a room in which
players congregate to start and join games. A lobby also comes with a chat
channel so players can coordinate game creation, etc.
"""

import twisted.spread.pb as pb
import PyQt4.QtGui as QtGui

import pyboard.util.networkModel as nm
import pyboard.util.cacheableTypes as ct
import pyboard.client.gameConfigs as gameConfigs

class LobbyControl(nm.Control):
    """Client input object for LobbyModel"""
    
    def view_createGame(self, creator, gameType, gameName, password):
        return self.obj.createGame(creator, gameType, gameName, password)
    
    def view_joinGame(self, user, gameName, password):
        return self.obj.joinGame(user, gameName, password)


class LobbyModel(nm.Model):
    
    controlClass = LobbyControl
    
    def __init__(self):
        nm.Model.__init__(self)
        self.users = ct.CacheableKeysMap()
        self.games = ct.CacheableKeysMap()
        self.loadGameClasses()
    
    def loadGameClasses(self):
        self.gameClasses = {}
        for name, info in gameConfigs.config.items():
            self.gameClasses[name] = info['gameModel']
    
    def stateForPerspective(self, perspective):
        d = {}
        d['users'] = self.users
        d['games'] = self.games
        d['availableGameTypes'] = self.gameClasses.keys()
        return d
    
    def addUser(self, user):
        """Add a user to the lobby
        
        If a user with user.name already exists then user is not added
        
        Return True if the user is added, otherwise False.
        """
        if user.name in self.users:
            return False
        else:
            self.users[user.name] = user
            return True
    
    def createGame(self, creator, gameType, gameName, password):
        """
        Client request to create new game
        
        creator - Avatar: avatar of client making the request.
        gameType - string: type of game to start.
        gameName - string: name of game if it is created
        password - string: password needed to join the game
        
        Returns (bool, message) where bool is True if the game is created and
        False otherwise. Message advises requesting client as to why game was
        not created, etc.
        """
        if gameName in self.games:
            return (False, "Game named %s already exists"%gameName)
        gc = self.gameClasses.get(gameType, None)
        if gc is None:
            return (False, "Unknown game type %s"%gameType)
        g = gc(gameName, creator, password)
        self.games[gameName] = g
        return (True, "Game %s of type %s created"%(gameName, gameType))
    
    def joinGame(self, user, gameName, password):
        """
        Join a user to an existing game
        
        Returns the desired game, or None if it doesn't exist.
        """
        g = self.games.get(gameName, None)
        if g:
            ok = g.authenticate(user, password)
            if ok:
                return g.model
            else:
                return None
        else:
            return None


class LobbyView(nm.View):
    """Client's view of the Lobby"""
    
    def __init__(self, graphics=None):
        self.graphics = None
    
    def receiveState(self, state):
        self.users = state['users']
        self.games = state['games']
        self.availableGameTypes = state['availableGameTypes']


pb.setUnjellyableForClass(LobbyModel, LobbyView)


class AddDiscardGraphics(QtGui.QListWidget):
    """A QListWidget that displays a set
    
    Normally instanced via promotion in QtDesigner.
    """
    def __init__(self, parent, logical=None, *args, **kw):
        super(AddDiscardGraphics, self).__init__(parent, *args, **kw)
        self.logical = logical
    
    def gotLogical(self, logical):
        self.logical = logical
        logical.add.addObserver(self.add)
        logical.discard.addObserver(self.discard)
        for item in logical:
            self.add(item)
    
    def add(self, item):
        self.addItem(item)
    
    def discard(self, item):
        self.takeItem(item)