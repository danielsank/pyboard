"""
Copyright (c) 2014 Daniel Sank

This file is part of pyboard, an open source library for playing board games
over the network.

This file launches a server.
"""

import logging

import pyboard.client.constants as C

import twisted.cred.portal as portal
import twisted.spread.pb as pb
import twisted.internet.reactor as reactor

import server

def main(port):
    #Logging
    logger = logging.getLogger("pyboard")
    logger.setLevel(logging.WARNING)
    handler = logging.FileHandler("log.txt", 'w')
    handler.setLevel(logging.WARNING)
    f = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(f)
    logger.addHandler(handler)
    
    s = server.Server()
    if True:
        checker = server.UniqueNoPassword(C.CRED_PATH, server=s)
    else:
        checker = server.UniqueFilePasswordDB(C.CRED_PATH, server=s)
    p = portal.Portal(s, [checker])
    pbFactory = pb.PBServerFactory(p)#, unsafeTracebacks=True)
    reactor.listenTCP(port, pbFactory)
    reactor.run()


if __name__ == "__main__":
    main(C.PORT)
