"""
Copyright (c) 2014 Daniel Sank

This file is part of pyboard, an open source library for playing board games
over the network.

This file contains classes for establishing a client connection to the server.
"""

import pyboard.client.constants as C

import twisted.spread.pb as pb
import twisted.cred.credentials as credentials

#Get setUnjellyableForClass definitions
import pyboard.server.lobby as lobby
import pyboard.examples.chat.chat as chat

#Named functions
from twisted.internet.defer import inlineCallbacks, returnValue
from pyboard.util.observed import event

class Client(pb.Referenceable):
    """I am the mind"""
    
    def __init__(self):
        self.perspective = None
        self.connectionInfo = {}
        self.factory = pb.PBClientFactory()
        self.lobby = None
        self.activeGames = {} #game name -> game
    
    @event
    def startConnecting(self):
        pass
    
    def connect(self, host, port, username, password):
        self.connectionInfo['host'] = host
        self.connectionInfo['port'] = port
        self.connectionInfo['username'] = username
        c = credentials.UsernamePassword(self.connectionInfo['username'],
                                         password)
        from twisted.internet import reactor
        reactor.connectTCP(self.connectionInfo['host'],
                           self.connectionInfo['port'],
                           self.factory)
        #Log in with self as the "mind"
        d = self.factory.login(c, self)
        d.addCallback(self.connected)
        # No errback here; let Connect's errback handle failed login.
        return d
    
    @inlineCallbacks
    def connected(self, perspective):
        if perspective:
            success = True
            self.perspective = perspective
            lobby = yield self.perspective.callRemote("getLobby")
            self.gotLobby(lobby)
            chat = yield self.perspective.callRemote("getChat")
            self.gotChat(chat)
        else:
            success = False
        returnValue(success)
    
    @event
    def gotLobby(self, lobby):
        self.lobby = lobby
    
    @event
    def gotChat(self, chat):
        self.chat = chat
    
    def createGame(self, gameType, gameName):
        if self.lobby:
            d = self.lobby.callRemote("createGame", gameType, gameName, "sesame")
            d.addCallback(self._cbCreateGame)
    
    def _cbCreateGame(self, resp):
        succeeded, message = resp
        print("client.py: Game creation succeeded: %s"%succeeded)
        print("client.py: %s"%message)
    
    def joinGame(self, gameName):
        if self.lobby:
            #p = player.PlayerClient()
            d = self.lobby.callRemote("joinGame", gameName, "sesame")
            d.addCallback(self.joinedGame)
    
    @event
    def joinedGame(self, g):
        if g is None:
            print("client.py: failed to join game")
            return
        self.activeGames[g.name] = g
