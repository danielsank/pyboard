"""
Copyright (c) 2014 Daniel Sank

This file is part of pyboard, an open source library for playing board games
over the network.

This is the main launcher file for game clients.
"""

import sys
import weakref

import PyQt4.QtGui as QtGui
import pyboard.util.qt4reactor as qt4reactor
import pyboard.client.client as client
import pyboard.client.qtGraphics.mainWindow as mainWindow

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    qt4reactor.install()
    from twisted.internet import reactor
    c = client.Client()
    clientWindow = mainWindow.MainWindow(weakref.proxy(c))
    c.startConnecting()
    reactor.run()
