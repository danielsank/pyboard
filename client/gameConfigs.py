import pyboard.examples.sandbox.game as sbGame
import pyboard.examples.ccg.game as ccgGame
import pyboard.examples.ccg.qtGraphics.game as ccgGameGraphics

CCG = {"gameModel":ccgGame.Game, 'gameGraphics':ccgGameGraphics.Game}
SANDBOX = {"gameModel":sbGame.GameModel, "gameGraphics":sbGame.GameWindow}

config = {
          "sandbox":SANDBOX,
          "ccg":CCG
          }