"""
Copyright (c) 2014 Daniel Sank

This file is part of pyboard, an open source library for playing board games
over the network.

This is a main window implementation using PyQt.

TODO:
We should probabily factor out the logic from the main window into an interface
to be used by various graphical implementations.
"""

import os
import weakref
import twisted.internet.defer as defer

import PyQt4.QtGui as QtGui
import PyQt4.uic as uic

import pyboard.client.qtGraphics.connect as connect
import pyboard.client.gameConfigs as gameConfigs

class MainWindow(QtGui.QMainWindow):
    """
    Main client gui window
    
    I think no real state info should live here
    """
    
    def __init__(self, client, *args, **kw):
        super(MainWindow, self).__init__(*args, **kw)
        self.client = client
        #Set up notifications from client
        self.client.startConnecting.addObserver(self.startConnecting)
        self.client.gotLobby.addObserver(self.gotLobby)
        self.client.gotChat.addObserver(self.gotChat)
        self.client.joinedGame.addObserver(self.joinedGame)
        
        self.initUi()
    
    def initUi(self):
        uic.loadUi(os.path.join(os.path.dirname(__file__),"mainWindow.ui"),\
            self)
        self.pushButtonCreateGame.clicked.connect(self.createGame)
        self.pushButtonJoinGame.clicked.connect(self.joinGame)
    
    def startConnecting(self):
        self.connector = connect.Connector(self.client)
    
    def createGame(self):
        gameType = str(self.gameTypeCombobox.currentText())
        if gameType:
            gameName, ok = QtGui.QInputDialog.getText(self, 'Game name',\
                                                      'Enter game name:')
            gameName = str(gameName)
            if gameName and ok:
                self.client.createGame(gameType, gameName)
    
    def gotLobby(self, lobbyLogical):
        self.show()
        self.gameTypeCombobox.addItems(lobbyLogical.availableGameTypes)
        self.listWidgetUsers.gotLogical(lobbyLogical.users)
        self.listWidgetGames.gotLogical(lobbyLogical.games)
    
    def gotChat(self, chat):
        self.chat.gotLogical(chat)
    
    def joinGame(self):
        item = self.listWidgetGames.currentItem()
        if item:
            gameName = str(item.text())
            self.client.joinGame(gameName)
    
    def joinedGame(self, g):
        gameGraphics = gameConfigs.config[g.type]['gameGraphics'](self)
        self.tabWidget.addTab(gameGraphics, g.name)
        gameGraphics.gotGame(g)
