import os
import pyboard.client.constants as C

import PyQt4.QtGui as QtGui
import PyQt4.uic as uic    

class Connector(QtGui.QWidget):
    def __init__(self, client, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.client = client
        uic.loadUi(os.path.join(os.path.dirname(__file__),"connect.ui"),\
            self)
        self.pushButtonConnect.clicked.connect(self.done)
        self.lineEditPort.setText(str(C.PORT))
        self.lineEditHost.setText('localhost')
        
        self.lineEditUsername.returnPressed.connect(self.done)
        self.lineEditPassword.returnPressed.connect(self.done)
        self.lineEditPort.returnPressed.connect(self.done)
        self.lineEditHost.returnPressed.connect(self.done)
        
        self.show()
    
    def done(self):
        port = int(self.lineEditPort.text())
        host = str(self.lineEditHost.text())
        username = str(self.lineEditUsername.text())
        password = str(self.lineEditPassword.text())
        d = self.client.connect(host, port, username, password)
        d.addCallback(self.doneConnecting)
    
    def doneConnecting(self, success):
        if success:
            self.hide()
        else:
            print("Connection failed")

