"""
Copyright (c) 2014 Daniel Sank

This file is part of pyboard, an open source library for playing board games
over the network.

pyboard uses remote objects to synchronize game state between the server and
multiple client processes. To make it easy to customize the user interface, and
to allow different graphical backend libraries to be used for the same set of
objects, we need to separate the game objects from the classes that represent
them. In particular, different instances of the same object might need to be
represented in different ways in different parts of the user interface.

A good solution to this problem is to give each game object a set of
"observers". These observers are callable python objects (ie. functions or
bound methods) that should be invoked whenever something happens to the game
object. Here's an example

class MyObservedObject(<base classes>):
    def __init__(self):
        self.observers = set()
    def foo(self, a, b):
        for o in self.observers:
            o(a, b)
    def bar(self, c, d):
        for o in self.observers:
            o(c, d)
    def addObserver(self, o):
        self.observers.add(o)

This solution has problems. For example, the observed methods share a common
set of observers. This is very restrictive. For instance, it means that all
observers have to have the same call signature.

We can improve the situation like this

class MyObservedObject(object):
    def __init__(self):
        self.observers = {} #method name -> observers
    def foo(self, a, b):
        for o in self.observers['foo']:
            o(a, b)
    def bar(self, c):
        for o in self.observers['bar']:
            o(c)
    def addObserver(self, o, methodName):
        s = self.observers.setdefault(methodName, set())
        s.add(o)

You can test it with the following code:

===EXAMPLE 1===
class MyObservedObject(object):
    def __init__(self):
        self.observers = {} #method name -> observers
    def foo(self, a, b):
        print("foo got %s and %s"%(a, b))
        if 'bar' in self.observers:
            for o in self.observers['foo']:
                o(a, b)
    def bar(self, c):
        print("bar got %s"%(c,))
        if 'bar' in self.observers:
            for o in self.observers['bar']:
                o(c)
    def addObserver(self, o, methodName):
        s = self.observers.setdefault(methodName, set())
        s.add(o)

def observer_foo(a, b):
    print("observer_foo got %s and %s"%(a,b))
def observer_bar(c):
    print("observer_bar got %s"%c)
moo = MyObservedObject()
moo.addObserver(observer_foo, 'foo')
moo.addObserver(observer_bar, 'bar')
moo.foo('apple', 'banana')
moo.bar(None)
===============

If you run the example you get the following output
> foo got apple and banana
> observer_foo got apple and banana
> bar got None
> observer_bar got None

That's not bad. We susribed specific observer functions to specific events on
the observed object. There are, however, two substantial ways in which our
solution isn't quite pythonic/elegant, as I will now explain.

(As we procede to address these issues, remember that everything we discuss
from here on is essentially in persuit of syntactic sugar. The solution we
already presented not only works but is easy to understand and fundamentally
simple.)

1 (OBJECT ORIENTATION). In our code so far we register as observers by calling
a function with the name of the method we want to observe passed in as an
argument. We'd rather do it by invoking a method on an actual object. For
example, rather than doing

moo.addObserver(observer_foo, 'foo')

we'd rather do

moo.foo.addObserver(observer_foo)

because in the second case we handle exactly the objects we care about, without
using strings to hook things together. We call this the "object orientation"
issue.

2 (PACKAGING). The second issue is that in the example we put

if <method name> in self.observers:
    for o in self.observers[<method name>]:
        o(<args and keyword args>)

Not only has the callback code really invaded our class's methods, but we have
to repeatedly type this code into each method we want to expose as an
observable event. We call this the "packaging" issue.

Let's address the packaging issue first. One solution would be to simply
define a function:

def updateObservers(obj, methodName, *arg, **kw):
    if methodName in obj.observers:
        for o in obj.observers[methodName]:
            o(*arg, **kw)

This turns

def foo(self, a, b):
    print("foo got %s and %s"%(a, b))
    if 'foo' in self.observers:
        for o in self.observers['foo']:
            o(a, b)

into

def foo(self, a, b):
   print("foo got %s and %s"%(a, b))
   updateObservers(self, 'foo', a, b)

That's more concise, but we still have to pass the method name, and we still
have an extra line of code in each method that we want to make observable. We
can fix both of these problems with some functional programming:

def event(func):
    def wrapped(obj, *arg, **kw):
        '''I call func and then all of its observers'''
        func(obj, *arg, **kw)
        updateObservers(obj, func.__name__, *arg, **kw)
    return wrapped

Now we can just do this

def foo(self, a, b):
    print("foo got %s and %s"%(a, b))
foo = event(foo)

or even better, using event as a decorator,

@event
def foo(self, a, b):
    print("foo got %s and %s"%(a, b))

===EXAMPLE 2===
def event(func):
    def wrapped(obj, *arg, **kw):
        '''I call func and then all of its observers'''
        func(obj, *arg, **kw)
        updateObservers(obj, func.__name__, *arg, **kw)
    return wrapped
    
def updateObservers(obj, methodName, *arg, **kw):
    if methodName in obj.observers:
        for o in obj.observers[methodName]:
            o(*arg, **kw)

class MyObservedObject(object):
    def __init__(self):
        self.observers = {} #method name -> observers
    @event
    def foo(self, a, b):
        print("foo got %s and %s"%(a, b))
    @event
    def bar(self, c):
        print("bar got %s"%(c,))
    def addObserver(self, o, methodName):
        s = self.observers.setdefault(methodName, set())
        s.add(o)

def observer_foo(a, b):
    print("observer_foo got %s and %s"%(a,b))
def observer_bar(c):
    print("observer_bar got %s"%c)
moo = MyObservedObject()
moo.addObserver(observer_foo, 'foo')
moo.addObserver(observer_bar, 'bar')
moo.foo('apple', 'banana')
moo.bar(None)
===============

In order to make our event handling code reusable we just factor it into a base
class.

class Observed(object):
    '''Mixin class for observable objects'''
    def __init__(self):
        self.observers = {}
    def addObserver(self, observer, methodName):
        s = self.observers.setdefault(methodName, set())
        s.add(observer)

We then rewrite MyObservedObject as

class MyObservedObject(Observed):
    def __init__(self):
        <code specific to this class>
        Observed.__init__(self)
    @event
    def foo(self, a, b):
        print("foo got %s and %s"%(a, b))
    <etc.>

We've now completely solved the packaging issue, because the event code is
neatly packaged into a decorator and a subclass. We now turn to the object
orientation issue. 

TODO - Explain aboiut weak references and how we fix it with the Callback
       class.
TODO - write the object orientation part and actually implement it.
"""

import functools
import weakref

def event(func):
    """Makes a method notify registered observers"""
    return ObservableMethodDescriptor(func)


class ObservableMethod(object):
    """This has to behave like a bound method is all possible ways"""
    
    def __init__(self, obj, func):
        self.func = func
        functools.update_wrapper(self, func)
        self.objectWeakRef = weakref.ref(obj)
        self._callbacks = weakref.WeakKeyDictionary() #obj -> methodNames
    
    def addObserver(self, boundMethod):
        s = self._callbacks.setdefault(boundMethod.__self__, set())
        s.add(boundMethod.__name__)
    
    def discardObserver(self, boundMethod):
        if boundMethod.__self__ in self._callbacks:
            self._callbacks[boundMethod.__self__].discard(boundMethod.__name__)
    
    def __call__(self, *arg, **kw):
        result = self.func(self.objectWeakRef(), *arg, **kw)
        for obj in self._callbacks:
            for methodName in self._callbacks[obj]:
                getattr(obj, methodName)(*arg, **kw)
        return result
    
    @property
    def __self__(self):
        """Get a strong reference to the object owning this ObservableMethod"""
        return self.objectWeakRef()


class ObservableMethodDescriptor(object):
    
    def __init__(self, func):
        """
        To each instance of the class using this descriptor, we associate a
        Callback object to help determine what do with when we're accessed.
        """
        self.instances = weakref.WeakKeyDictionary() #instance -> callbacks
        self._func = func
    
    def __get__(self, inst, cls):
        # Cannot use setdefault here because that would construct a new
        # instance of ObservableMethod every time this code block runs.
        if inst in self.instances:
            om = self.instances[inst]
        else:
            om = ObservableMethod(inst, self._func)
            self.instances[inst] = om
        return om
    
    def __set__(self, inst, val):
        raise RuntimeError("Assigning to ObservableMethod not supported")


class ObservedMeta(type):
    """Use me to patch methods from a superclass"""
    def __new__(cls, name, bases, d):
        def findMethodFunction(m):
            if m in d:
                return d[m]
            for base in bases:
                try:
                    return getattr(base, m).im_func
                except AttributeError:
                    pass
            raise AttributeError("No bases of %s have method %s"%(name, m))
        for m in d['_eventMethods']:
            d[m] = event(findMethodFunction(m))
        return type(name, bases, d)


def test():
    
    class Foo(object):
        def __init__(self, name):
            self.name = name
        
        @event
        def bar(self):
            print("object %s says 'bar'"%self.name)
        
        def baz(self):
            print("object %s says 'baz'"%self.name)
    
    
    a = Foo('a')
    b = Foo('b')
    
    a.bar.addObserver(b.bar)
    a.bar.addObserver(b.baz)
    a.bar()
    
    del b
    a.bar()


if __name__ == "__main__":
    test()

