"""
Copyright (c) 2013, 2014 Daniel Sank

This file is part of pyboard, an open source library for playing board games
over the network.

This file implements the model/view/control system using objects from
Twisted's in perspective broker library.

There are three important classes, Model, View, and Control. The Model is the
class that fundamentally represents an object. It lives on the server. Model
is a subclass of pb.Cacheable.

The View is the client's representation of a Model. It is a subclass of
pb.RemoteCache

The Control is the thing upon which clients can remotely invoke methods. We
automagically pass remote references to the Control whenever we give out a
View. If the client has an instance of a View called myView, it can invoke a
method on its remote reference to the Control by calling
myView.callRemote(...). This call is redirected to the remote reference to the
Control.
"""

import weakref

import twisted.spread.pb as pb
from twisted.internet.defer import inlineCallbacks, returnValue

class Model(pb.Cacheable):
    
    controlClass = None
    
    def __init__(self, parent, model=None):
        self._control = self.controlClass(model if model else self)
        self.observers = weakref.WeakKeyDictionary() # perspective -> observer
    
    def getStateToCacheAndObserveFor(self, perspective, observer):
        self.observers[perspective] = observer
        state = {}
        state['_control'] = self._control
        userDefinedState = self.stateForPerspective(perspective)
        state.update(userDefinedState)
        return state
    
    def stateForPerspective(self, perspective):
        """Expose state information to a particular perspective"""
        raise NotImplementedError
    
    @property
    def model(self):
        """Compatibility with networkModel.Networkable"""
        return self


class View(pb.RemoteCache):
    
    @inlineCallbacks
    def callRemote(self, *arg, **kw):
        result = yield self._control.callRemote(*arg, **kw)
        returnValue(result)
    
    def setCopyableState(self, state):
        self._control = state.pop('_control')
        self.receiveState(state)
    
    def receiveState(self, state):
        """
        Handle state delegated by server
        
        Must override in subclass
        """
        raise NotImpelementedError


class Control(pb.Viewable):
    def __init__(self, obj):
        self.model = weakref.proxy(obj, self.proxyCallback)
    
    def proxyCallback(self, ref):
        pass


Cacheable = Model

