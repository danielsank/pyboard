import twisted.spread.pb as pb
import pyboard.util.observed as observed

import weakref

class Model(pb.Cacheable):
    
    controlClass = None
    
    def __init__(self, obj):
        self.obj = weakref.proxy(obj)
        self._control = self.controlClass(obj)
        self.observers = weakref.WeakKeyDictionary # perspective -> observer
    
    def getStateToCacheAndObserveFor(self, perspective, observer):
        self.observers[perspective] = observer
        state = {}
        state['_control'] = self._control
        userDefinedState = self.stateForPerspective(perspective)
        state.update(userDefinedState)
        return state
    
    def stateForPerspective(self, perspective):
        """Expose state information to a particular perspective"""
        raise NotImplementedError


class View(pb.RemoteCache):
    
    @inlineCallbacks
    def callRemote(self, *arg, **kw):
        result = yield self._control.callRemote(*arg, **kw)
        returnValue(result)
    
    def setCopyableState(self, state):
        self._control = state.pop('_control')
        self.receiveState(state)
    
    def receiveState(self, state):
        """
        Handle state delegated by server
        
        Must override in subclass
        """
        raise NotImpelementedError


class Control(pb.Viewable):
    def __init__(self, model):
        self.model = model


class A(object):
    
    def __init__(self, name):
        self.name = name
    
    def __str__(self):
        return self.name
    
    @observed.event
    def foo(self, x):
        print("%s foo'd %s"%(self.name, x))

def main():
    a = A('Alice')
    b = A('Bianca')
    a.addObserver('foo', b, 'foo')
    a.foo(23)
    for k, v in a._Observed__observers.items():
        print k
        print v.values()
    del k, v
    del b
    a.foo(44)
    for k, v in a._Observed__observers.items():
        print k
        print v.values()

if __name__ == "__main__":
    main()
