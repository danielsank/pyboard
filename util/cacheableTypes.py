"""
Copyright (c) 2014 Daniel Sank

This file is part of pyboard, an open source library for playing board games
over the network.

This file provides classes to allow network sharing of a few basic python
types.
"""

import twisted.spread.pb as pb
import collections
import pyboard.util.networkModel as nm

from pyboard.util.observed import event

#Wrapper classes that forward an interface to an underlying built-in type.

class DictBase(collections.MutableMapping):
    
    #Dict interface
    def __init__(self):
        """Don't allow initial data because it would bypass notifications"""
        self._d = dict()
    
    def __delitem__(self, key):
        return self._d.__delitem__(key)
    
    def __getitem__(self, key):
        return self._d[key]
    
    def __iter__(self):
        return self._d.__iter__()
    
    def __len__(self):
        return self._d.__len__()
    
    def __setitem__(self, key, val):
        return self._d.__setitem__(key, val)


class ObservableDict(DictBase):
    
    def __init__(self):
        DictBase.__init__(self)
    
    @event
    def __delitem__(self, key):
        DictBase.__delitem__(self, key)
    
    @event
    def __setitem(self, key, val):
        DictBase.__setitem__(self, key, val)


class NotifyingDict(DictBase):
    def __delitem__(self, key):
        result = DictBase.__delitem__(self, key)
        self.on_delitem(key)
        return result
    
    def __setitem__(self, key, val):
        result = DictBase.__setitem__(self, key, val)
        self.on_setitem(key, val)
        return result
    
    def on_delitem(self, key):
        raise NotImplementedError
    
    def on_setitem(self, key, val):
        raise NotImplementedError


class SetBase(collections.MutableSet):
    def __init__(self):
        self._s = set()
    
    def __contains__(self, obj):
        return self._s.__contains__(obj)
    
    def __iter__(self):
        return self._s.__iter__()
    
    def __len__(self):
        return self._s.__len__()
    
    def add(self, obj):
        return self._s.add(obj)
    
    def discard(self, obj):
        return self._s.discard(obj)


class ObservableSet(SetBase):
    
    def __init__(self):
        SetBase.__init__(self)
    
    @event
    def add(self, obj):
        return SetBase.add(self, obj)
    
    @event
    def discard(self, obj):
        return SetBase.discard(self, obj)


class NotifyingSet(SetBase):
    def add(self, obj):
        """Overidden from base class"""
        result = SetBase.add(self, obj)
        self.on_add(obj)
        return result
    
    def discard(self, obj):
        """Overridden from base class"""
        result = SetBase.add(self, obj)
        self.on_discard(obj)
        return result
    
    def on_add(self, obj):
        raise NotImplementedError
    
    def on_discard(self, obj):
        raise NotImplementedError


class ListBase(collections.MutableSequence):
    
    def __init__(self, data=None):
        if data is not None:
            self._l = [data]
        else:
            self._l = []
    
    def __delitem__(self, i):
        del self._l[i]
    
    def __getitem__(self, i):
        return self._l[i]
    
    def __len__(self):
        return len(self._l)
    
    def __setitem__(self, i, data):
        self._l[i] = data
    
    def insert(self, i, data):
        self._l.insert(i, data)
    
    def __str__(self):
        return str(self._l)


class ObservableList(ListBase):
    
    @event
    def __delitem__(self, i):
        return ListBase.__delitem__(self, i)
    
    @event
    def __getitem__(self, i):
        return ListBase.__getitem__(self, i)
    
    @event
    def __setitem__(self, i, data):
        return ListBase.__setitem__(self, i, data)
    
    @event
    def insert(self, i, data):
        return ListBase.insert(self, i, data)


class NotifyingList(ListBase):
    def __delitem__(self, i):
        result = ListBase.__delitem__(self, i)
        self.onDelItem(i)
        return result
    
    def __setitem__(self, i, data):
        result = ListBase.__setitem__(self, i, data)
        self.onSetItem(i, data)
        return result
    
    def insert(self, i, data):
        result = ListBase.insert(self, i, data)
        self.onInsert(i, data)
        return result
    
    def onDelItem(self, i):
        raise NotImplementedError
    
    def onSetItem(self, i, data):
        raise NotImplementedError
    
    def onInsert(self, i, data):
        raise NotImplementedError


# Networkable types

class SetControl(nm.Control):
    pass


class SetModel(nm.Model):
    
    controlClass = SetControl
    
    def __init__(self, obj):
        nm.Model.__init__(self, obj)
        self.obj.addObserver("add", self.add)
        self.obj.addObserver("discard", self.discard)
    
    def add(self, obj):
        raise NotImplementedError
    
    def discard(self, obj):
        raise NotImplementedError
    
    def stateForPerspective(self, perspective):
        return {'s': self.obj._s}


class SetView(nm.View, ObservableSet):
    
    def __init__(self):
        ObservableSet.__init__(self)
    
    def receiveState(self, state):
        for item in state:
            self.add(item)


pb.setUnjellyableForClass(SetModel, SetView)


class NetworkableSet(SetBase, nm.Networkable):
    """
    Convenience class for networkable sets
    
    Only use this if your networkable object has no functionality beyond that
    of a vanilla set.
    """
    
    def __init__(self):
        SetBase.__init__(self)
        nm.Networkable.__init__(self)
    
    @event
    def add(self, obj):
        SetBase.add(self, obj)
    
    @event
    def discard(self, obj):
        SetBase.discard(self, obj)
    
    def remodel(self):
        nm.Networkable.remodel(self)
        for obj in self:
            obj.remodel()

#Cacheable types with interfaces matching built-ins

class CacheableMap(pb.Cacheable, NotifyingDict, collections.Hashable):
    """Cacheable map (not dict) that publishes both keys and values
    
    Remote caches of this object must implement two methods
    1. observe_setitem(key, val). Called when an item is added to our _d.
    2. observe_delitem(key). Called when we pop an item from our _d.
    
    Notes:
    Implementation of hash, required for pb.Cacheable, requires compatible 
    implementation of __eq__, etc. Because of this, comparison of instances
    of this class only return True if comparing an instance with itself. This
    is NOT dict-like behavior so watch out.
    """
    
    def __init__(self):
        NotifyingDict.__init__(self)
        self.observers = set()
    
    def getStateToCacheAndObserveFor(self, perspective, observer):
        self.observers.add(observer)
        return dict(self.items())
    
    def on_setitem(self, key, val):
        for o in self.observers:
            o.callRemote("setitem", key, val)
    
    def on_delitem(self, key):
        for o in self.observers:
            o.callRemote("delitem", key)
    
    #Hashable interface
    def __hash__(self):
        return hash(id(self))
    
    def __eq__(self, other):
        return id(self) == id(other)
    
    def __ne__(self, other):
        return not self.__eq__(other)


class CacheableKeysMap(pb.Cacheable, NotifyingDict, collections.Hashable):
    """Cacheable map that publishes only keys
    
    Because only keys are sent, we assume the remote cache of this object is
    something like a set. Therefore we require the remote cache to implement
    the following two methods:
    1. observe_add(key, val)
    2. observe_discard(key)
    
    Notes:
    We implement __hash__ as hash(self.id). This forces us to override __eq__
    so that comparison now only return True if comparing the same object.
    This is not dict-like behavior.
    """
    
    def __init__(self):
        NotifyingDict.__init__(self)
        self.observers = set()
    
    def getStateToCacheAndObserveFor(self, perspective, observer):
        self.observers.add(observer)
        return self.keys()
        
    def on_setitem(self, key, val):
        for o in self.observers:
            o.callRemote("add", key)
    
    def on_delitem(self, key):
        for o in self.observers:
            o.callRemote("discard", key)
    
    def stoppedObserving(self, perspective, observer):
        self.observers.discard(observer)
    
    #Hashable interface
    def __hash__(self):
        return hash(id(self))
    
    def __eq__(self, other):
        return id(self) == id(other)
    
    def __ne__(self, other):
        return not self.__eq__(other)


class CacheableSet(pb.Cacheable, NotifyingSet, collections.Hashable):
    def __init__(self):
        NotifyingSet.__init__(self)
        self.observers = []#dict() #observer -> perspective
    
    def getStateToCacheAndObserveFor(self, perspective, observer):
        self.observers.append(observer)#self.observers[observer] = perspective
        return self._s
    
    def on_add(self, obj):
        for o in self.observers:
            o.callRemote("add", obj)
    
    def on_discard(self, obj):
        for o in self.observers:
            o.callRemote("discard", obj)
    
    def stoppedObserving(self, perspective, observer):
        self.observers.pop(observer)
    
    #Hashable interface
    def __hash__(self):
        return hash(id(self))
    
    def __eq__(self, other):
        return id(self) == id(other)
    
    def __neq__(self, other):
        return not self.__eq__(other)


class CacheableList(pb.Cacheable, NotifyingList, collections.Hashable):
    def __init__(self, data=None):
        NotifyingList.__init__(self, data=data)
        self.observers = set()
    
    def getStateToCacheAndObserveFor(self, perspective, observer):
        self.observers.add(observer)
        return self._l
    
    def onDelItem(self, i):
        for o in self.observers:
            o.callRemote("delItem", i)
    
    def onSetItem(self, i, data):
        for o in self.observers:
            o.callRemote("setItem", i, data)
    
    def onInsert(self, i, data):
        for o in self.observers:
            o.callRemote("insert", i, data)
    
    def stoppedObserving(self, perspective, observer):
        self.observers.discard(observer)
    
    #Hashable interface
    def __hash__(self):
        return hash(id(self))
    
    def __eq__(self, other):
        return id(self) == id(other)
    
    def __neq__(self, other):
        return not self.__eq__(other)


#Remote caches for cacheable types

class MapRemoteCache(pb.RemoteCache, DictBase):
    """Remote cache for CacheableMap
    
    Todo:
    Disallow mutations by client
    """
    
    def setCopyableState(self, state):
        for k, v in state.items():
            self[k] = v
    
    def observe_setitem(self, key, val):
        self[key] = val
    
    def observe_delitem(self, key):
        self.__delitem__(key)


class ObservedMapRemoteCache(MapRemoteCache):
    def __init__(self):
        MapRemoteCache.__init__(self)
    
    @event
    def __setitem__(self, key, value):
        MapRemoteCache.__setitem__(self, key, value)


pb.setUnjellyableForClass(CacheableMap, ObservedMapRemoteCache)


class SetRemoteCache(pb.RemoteCache, SetBase):
    """Remote cache for set-like data
    
    Todo
    Disallow mutation by client
    """
    
    def setCopyableState(self, state):
        for obj in state:
            self.add(obj)
    
    def observe_add(self, obj):
        self.add(obj)
    
    def observe_discard(self, obj):
        self.discard(obj)


class ObservedSetRemoteCache(SetRemoteCache):
    def __init__(self):
        SetRemoteCache.__init__(self)
    
    @event
    def add(self, obj):
        SetRemoteCache.add(self, obj)
    
    @event
    def discard(self, obj):
        setRemoteCache.discard(self, obj)


pb.setUnjellyableForClass(CacheableKeysMap, ObservedSetRemoteCache)
pb.setUnjellyableForClass(CacheableSet, ObservedSetRemoteCache)


class ListRemoteCache(pb.RemoteCache, ListBase):
    def setCopyableState(self, state):
        self._l = state
    
    def observe_delItem(self, i):
        self.__delitem__(i)
    
    def observe_setItem(self, i, data):
        self.__setitem__(i, data)
    
    def observe_insert(self, i, data):
        self.insert(i, data)


class ObservedListRemoteCache(ListRemoteCache):
    def __init__(self):
        ListRemoteCache.__init__(self)
    
    @event
    def __delitem__(self, i):
        ListRemoteCache.__delitem__(self, i)
    
    @event
    def __setitem__(self, i, data):
        ListRemoteCache.__setitem__(self, i, data)
    
    @event
    def insert(self, i, data):
        ListRemoteCache.insert(self, i, data)


pb.setUnjellyableForClass(CacheableList, ObservedListRemoteCache)
