"""
Copyright (c) 2013, 2014 Daniel Sank

This file is part of pyboard, an open source library for playing board games
over the network.

This file implements the model/view/control system using objects from
Twisted's in perspective broker library.

There are three network classes: Model, View, and Control, and one non-network
class: Networkable.

Networkable lives on the server and knowns nothing about the network. It can
spawn instances of Model to share over the network.

Model is a class that adapts an object to the network. Each instance of Model
has a .obj attribute which is a non-network object, possibly an instance of
Networkable. Each Model instance should find a way to listen for changes on
its .obj so that it can pass those changes over the network.

The View is the client's representation of a Model. When a Model detects a
change in its .obj, it sends updates to each View associated to it.

The Control is the thing upon which clients can remotely invoke methods. When
a View is passed over the network it comes with a remote reference to a
Control. When the client invokes .callRemote on its View, the call is actually
passed though to the Control.
"""

import weakref
import twisted.spread.pb as pb

from twisted.internet.defer import inlineCallbacks, returnValue

class MissingClassError(object):
    def __init__(self, label):
        self.label = label
    
    def __call__(self, *arg, **kw):
        raise Exception("No class specified for %s"%(self.label,))


class Model(pb.Cacheable):
    
    controlClass = MissingClassError('controlClass')
    
    def __init__(self, obj=None):
        if obj is None:
            obj = self
        self.obj = weakref.proxy(obj)
        self.__control = self.controlClass(obj)
        self.observers = weakref.WeakKeyDictionary() # perspective -> observer
    
    def getStateToCacheAndObserveFor(self, perspective, observer):
        self.observers[perspective] = observer
        state = {}
        state['_control'] = self.__control
        userDefinedState = self.stateForPerspective(perspective)
        state.update(userDefinedState)
        return state
    
    def stateForPerspective(self, perspective):
        """Expose state information to a particular perspective"""
        raise NotImplementedError
    
    @property
    def model(self):
        return self


class View(pb.RemoteCache):
    
    @inlineCallbacks
    def callRemote(self, *arg, **kw):
        result = yield self.__control.callRemote(*arg, **kw)
        returnValue(result)
    
    def setCopyableState(self, state):
        self.__control = state.pop('_control')
        self.receiveState(state)
    
    def receiveState(self, state):
        """Handle state delegated by server"""
        raise NotImpelementedError


class Control(pb.Viewable):
    
    def __init__(self, obj):
        self.obj = weakref.proxy(obj, self.proxyCallback)
    
    def proxyCallback(self, ref):
        pass


class Networkable(object):
    
    modelClass = MissingClassError('modelClass')
    snapshotClass = MissingClassError('copyClass')
    
    def __init__(self):
        self._model = None
    
    @property
    def model(self):
        if not self._model:
            self._model = self.modelClass(self)
        return self._model
    
    def snapshot(self):
        return self.snapshotClass(self)
    
    def remodel(self):
        """
        Reset network model
        
        If this is a container object, this method should probably call remodel
        on the contained objects.
        """
        self._model = None


